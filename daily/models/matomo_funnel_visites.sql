{{ config(materialized='table') }}

select idvisit, idvisitor
	, min(server_time) as server_time
	, case when max(visita_producte_servei_venedora)=1 then 'Visita producte, servei o venedora'
	    else 'NO visita producte, servei o venedora' end as visita_producte_servei_venedora
	, case when max(omple_carret)=1 then 'Omple cistella' else 'NO omple cistella' end as omple_carret
	, case when max(compra)=1 then 'Compra' else 'NO compra' end as compra
from (
SELECT v.idvisit, v.idvisitor, va.server_time
,case when acs.name like '%producte%' or acs.name like '%servei%' or acs.name like '%vendedores%'
		then 1
		else 0 end
	as visita_producte_servei_venedora
,case when c.idvisit is not null then 1 else 0 end as omple_carret
,case when c.idorder is not null then 1 else 0 end as compra
FROM matomo_db_matomo_log_visit v
	join matomo_db_matomo_log_link_visit_action va on va.idvisit =v.idvisit
	join matomo_db_matomo_log_action acs on acs.idaction =va.idaction_url
	 left JOIN matomo_db_matomo_log_conversion c ON v.idvisit = c.idvisit
where v.idsite=3
) a
group by idvisit, idvisitor